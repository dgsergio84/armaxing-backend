/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.armaxing.cloud.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 *
 * @author sergio
 */
@Configuration
@ConfigurationProperties("spring.datasource")
public class DBConfig {
    private static final Logger LOG = LoggerFactory.getLogger(DBConfig.class);
    
    private String driverClassName;
    private String url;

    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    @Profile("dev")
    @Bean
    public String devDatabaseConnection(){
        LOG.info("DB connection for DEVELOPMENT profile");
        LOG.info(driverClassName);
        LOG.info(url);
        
        return "DB connection for DEVELOPMENT profile";
    }
    
}
