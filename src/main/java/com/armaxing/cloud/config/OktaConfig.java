/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.armaxing.cloud.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 *
 * @author sergio
 */
@Component("oktaConfig")
@Configuration
@ConfigurationProperties("okta.oauth2")
public class OktaConfig {
    
    private static final Logger LOG = LoggerFactory.getLogger(OktaConfig.class);
    
    private String orgUrl;
    private String token;
    // private String clientId;

    public OktaConfig() {
    }

    public String getOrgUrl() {
        return orgUrl;
    }

    public void setOrgUrl(String orgUrl) {
        this.orgUrl = orgUrl;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    /*
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    */
    
    @Bean
    public String oktaConfiguration(){
        LOG.info("Okta API configuration");
        LOG.info("Okta Org. URL: {}",orgUrl);
        // LOG.info(token);
        // LOG.info("Dep. Nom. Web App Okta client Id: {}",clientId);
        
        return "Okta - API configuration";
    }
    
}
