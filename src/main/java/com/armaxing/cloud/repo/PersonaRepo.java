/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.armaxing.cloud.repo;

import java.util.Optional;

import com.armaxing.cloud.entidad.Persona;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author sergio
 */
@Repository("prsonaRepo")
public interface PersonaRepo extends JpaRepository<Persona, Integer>{
    
    Optional<Persona> findByEmail(String email);

}
