/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.armaxing.cloud.controlador;

import java.net.URI;

import com.armaxing.cloud.config.OktaConfig;
import com.armaxing.cloud.entidad.Persona;
import com.armaxing.cloud.servicio.PersonaServicio;
import com.okta.sdk.authc.credentials.TokenClientCredentials;
import com.okta.sdk.client.Client;
import com.okta.sdk.client.ClientBuilder;
import com.okta.sdk.client.Clients;
import com.okta.sdk.resource.user.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author sergio
 */
@RestController
@RequestMapping(value = "/personas", produces = {"application/json"})
public class PersonaControlador {
    
    @Autowired
    PersonaServicio personaServicio;

    @Autowired
    OktaConfig oktaConfig;
    
    @GetMapping(path = "/select")
    public Iterable<Persona> getAllPersonas(){
        return personaServicio.findAllPersonas();
    }

    @GetMapping(path = "/person/{email}")
    public ResponseEntity<Persona> getPersonaBy(@PathVariable("email") String email){
        Persona persona = personaServicio.findPersonaPorEmail(email);
        if(persona == null)
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok(persona);
    }

    @GetMapping(path = "/user/{email}")
    public ResponseEntity<User> getUserNyEmail(@PathVariable("email") String email){
        ClientBuilder builder = Clients.builder()
            .setClientCredentials(new TokenClientCredentials(oktaConfig.getToken()))
            .setOrgUrl(oktaConfig.getOrgUrl());
        Client client = builder.build();
        
        return ResponseEntity.ok(client.getUser(email));
    }
 
    @GetMapping
    public Page<Persona> getAllPersonasPaged(@PageableDefault(page = 0, size = 5 )
                                                @SortDefault.SortDefaults({
                                                    @SortDefault(sort = "id", direction = Sort.Direction.ASC)
                                                })
                                            Pageable pageable
                                            // , @RequestParam(value = "id", required = false) Integer num
                                            , @RequestParam(value = "nombre", required = false) String nombre
                                            , @RequestParam(value = "ape1", required = false) String ape1
                                            , @RequestParam(value = "ape2", required = false) String ape2
                                            , @RequestParam(value = "email", required = false) String email
                                            , @RequestParam(value = "curp", required = false) String curp){
    
        return personaServicio.findAllPersonasPaged(pageable);   
    }
    
    @PostMapping
    public ResponseEntity<Persona> createUsusarioPersona(@RequestBody Persona persona){
    
        try{
            personaServicio.registraUsuarioPersona(persona);

            URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{email}")
                .buildAndExpand(persona.getEmail())
                .toUri();
            
            return ResponseEntity.created(location).build();
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

}
