/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.armaxing.cloud.servicio;

import com.armaxing.cloud.config.OktaConfig;
import com.armaxing.cloud.entidad.Persona;
import com.armaxing.cloud.repo.PersonaRepo;

import java.util.Optional;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.okta.sdk.authc.credentials.TokenClientCredentials;
import com.okta.sdk.client.Client;
import com.okta.sdk.client.ClientBuilder;
import com.okta.sdk.client.Clients;
import com.okta.sdk.resource.user.User;
import com.okta.sdk.resource.user.UserBuilder;

/**
 *
 * @author sergio
 */
@Transactional
@Service("personaServicio")
public class IPersonaServicio implements PersonaServicio {

    @Autowired
    OktaConfig oktaConfig;

    @Autowired
    PersonaRepo personaRepo;
    
    @Override
    public Iterable<Persona> findAllPersonas() {
        return personaRepo.findAll();
    }

    @Override
    public Page<Persona> findAllPersonasPaged(Pageable pageable) {
        return personaRepo.findAll(pageable);
    }
    
    @Override
    public void registraUsuarioPersona(Persona persona){

        Optional<Persona> optPersona = personaRepo.findByEmail(persona.getEmail());
        if(!optPersona.isPresent()){

            ClientBuilder builder = Clients.builder()
            .setClientCredentials(new TokenClientCredentials(oktaConfig.getToken()))
            .setOrgUrl(oktaConfig.getOrgUrl());
            Client client = builder.build();
            
            User user = UserBuilder.instance()
                        .setEmail(persona.getEmail())
                        .setFirstName(persona.getNombre())
                        .setLastName(persona.getApe1())
                        .setPassword(new char[]{'1','2','3','4'})
                        .buildAndCreate(client);
            // LOG.info("Okta credentials: {} - {}", oktaConfig.getToken(),oktaConfig.getOrgUrl());
            user.activate(true);
            
            personaRepo.save(persona);

        }

    }

    @Override
    public Persona findPersonaPorEmail(String email) {
        Optional<Persona> optPersona = personaRepo.findByEmail(email);
        if(optPersona.isPresent())
            return optPersona.get();
    
        return null;

    }

}
