/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.armaxing.cloud.servicio;

import com.armaxing.cloud.entidad.Persona;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author sergio
 */
public interface PersonaServicio {
    
    public Iterable<Persona> findAllPersonas();
    
    public Page<Persona> findAllPersonasPaged(Pageable pageable);

    public Persona findPersonaPorEmail(String email);

    public void registraUsuarioPersona(Persona persona);
}
